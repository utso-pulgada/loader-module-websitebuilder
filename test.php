<?php

	class test{

		public function createHTML(){

			$custom_style = '<style>'.$_POST['custom_style'].'</style>';
			$custom_js = '<script>'.$_POST['custom_js'].'</script>';

			$title = '<title>'.$_POST['template_name'].'</title>';

			$html = $_POST['production_html'];


			$raw_html = file_get_contents(__DIR__.'/html-ref.html');

			$raw_html = str_replace(['#{title}','#{custom_style}','#{html}','#{custom_js}'],[$title,$custom_style,$html,$custom_js], $raw_html);

			$myfile = fopen(__DIR__.'/'.$_POST['template_id'].".html", "w") or die("Unable to open file!");

			fwrite($myfile, $raw_html);
			fclose($myfile);


		}

	}

	header('Content-type:application/json');

	if( $_POST['template_id'] ){


		( new test )->createHTML();

		echo json_encode([ 'success' => true, 'msg' => 'Successfully created'] );
	}