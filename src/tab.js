import makeid from './makeid.js';

export default{
	build(arr,a=0){
		let headers = '';
		let contents = '';

		arr.forEach( function( item,index ) {
			let id = 'wbtab-'+makeid(5);
			let active = (a == index ) ? 'active' : '';
			headers+='<li class="'+active+' ref-index'+index+'"><a href="#'+id+'">'+item.header+'</a></li>'
			contents+='<div class="wbtab-content '+active+' ref-index'+index+'" id="'+id+'">'+item.contents+'</div>';
		});

		let html = `<div class="wbtab">
					<div class="wbtab-header">
					<ul>
					${headers}
					</ul>
					</div>
					<div class="wbtab-contents">${contents}</div>
					</div>`;

		return html;
	},
	init(obj){
		obj.querySelectorAll('.wbtab-header ul li a').forEach(function(item){
			item.removeEventListener('click',function(){});
			item.addEventListener('click',function(){
				if( obj.querySelector( '.wbtab-header li.active') ){
					obj.querySelector( '.wbtab-header li.active').classList.remove('active');
					this.closest('li').classList.add('active');
				}

				if( obj.querySelector( '.wbtab-contents .wbtab-content.active') ){
					obj.querySelector( '.wbtab-contents .wbtab-content.active').classList.remove('active');
					obj.querySelector( '.wbtab-contents '+obj.querySelector( '.wbtab-header li.active a').getAttribute('href') ).classList.add('active');
				}
			});
		});
	},
	addTab(tab,arr){
		let id = 'wbtab-'+makeid(5);
		let index = tab.querySelectorAll('.wbtab-header ul li').length;

		let newel = document.createElement('li');
			newel.innerHTML = '<a href="#'+id+'">'+arr.header+'</a>';
			newel.classList.add('ref-index'+index);

		let newel2 = document.createElement('div');
			newel2.classList.add('wbtab-content');
			newel2.setAttribute('id',id);
			newel2.innerHTML = arr.contents;
			newel2.classList.add('ref-index'+index);

		tab.querySelector('.wbtab-header ul').appendChild(newel);
		tab.querySelector('.wbtab-contents').appendChild(newel2);

		this.init(tab);

	},
	removeTab(tab){
		tab.querySelector('.wbtab-header ul li:last-child').remove();
		tab.querySelector('.wbtab-contents .wbtab-content:last-child').remove();

		if( !tab.querySelector('.wbtab-contents .wbtab-content.active') ){
			tab.querySelector('.wbtab-header ul li:first-child').classList.add('active');
			tab.querySelector('.wbtab-contents .wbtab-content:first-child').classList.add('active');
		}
	}

}