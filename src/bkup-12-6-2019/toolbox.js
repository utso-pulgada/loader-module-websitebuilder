import PouchDB from 'pouchdb-browser';
import elements from './elements.js'; // -- html interface
import errors from './errors.js' // -- error messages interface
import makeid from './makeid.js' // -- create random hash module
import modal from './modal.js'; // -- modal module
import elementsParser from './elementsParser.js';
import tools from './tools.js';
import $ from './external/jquery/jquery.js';
import './jquery-ui.min.js';
import droppable from './droppable.js'; // -- droppable

const entities = require("entities");

let db = new PouchDB('websiteeditor');

export default{
	templateDroppable(){
		droppable( $('.editor-body.droppable') );
	},
	toolSidebar(){
		let el = document.createElement('div');
				el.classList.add('editor-tool');
			el.innerHTML = '<a href="#" class="editor-tool-trigger"><i class="ti-plus"></i></a><div class="editor-tool-body"><div style="overflow-y:auto;overflow-x:hidden;height:100%;padding:0px 24px;">'+elementsParser.sidebarAccordions()+'</div></div>';
			el.querySelector('.editor-tool-trigger').addEventListener('click',function(){
				if( this.closest('.editor-tool').classList.contains('active') ){
					this.closest('.editor-tool').classList.remove('active');
				}else{
					this.closest('.editor-tool').classList.add('active');
				}
			});


		document.querySelector('body').appendChild(el);

		$( ".draggable" ).draggable({
			start : function(event,ui){
				event.target.style.background = '#ededed';
			},
			cursor: "crosshair",
			helper: "clone"
		});
	},
	tool( t ){
		const _self = this;

		let el = $('.droppable.active');


		if( el.length == 0 ){ // if not found then abort
			console.log('no active item found');
			return;
		}


		if( elements.toolList().findIndex( i => i.id == t ) == -1 ){
			return;
		}


		let html = ( elements.toolList().findIndex( i => i.id == t ) != -1 ) ? elements.toolList().find( i => i.id == t ).contents : '';
		let template_id = $('#template-editor').attr('data-templateid');
		let edit_tool = elements.tools('edit');
		let delete_tool = elements.tools('delete');

		db.get( template_id ).then(function(doc){
			let elements = doc.elements;
			let elementid = 'element-'+makeid(6)+elements.length;
			let active_elementid = false;
			
			el.prepend( html );

			if( el.hasClass('element') ){
				console.log(1);
				active_elementid = $('.droppable.active').attr('data-elementid');
			}else if( el.closest('.element').length > 0 ){
				console.log(2);
				active_elementid = el.closest( '.element' ).attr('data-elementid');
			}else{
				console.log(3);
				active_elementid = elementid;
			}

			el.find('.element').attr('data-elementid',elementid);



			el.find('.editable').append('<div class="content-editable" contentEditable="true"></div>');

			el.find('.with-tools').each(function(){

				let tools = '<div class="tools"><div>';
				if( $(this).hasClass('edit-tool') ){
					tools+=edit_tool;
				}
				if( $(this).hasClass('delete-tool') ){
					tools+=delete_tool;
				}
				tools+='</div></div>';

				$(this).prepend(tools);
			});


			el.find('.editable .tools .edit-tool').click(function(){
				 
				$('.editable').classList.remove('active');

				this.closest('.editable').classList.add('active');
				tools.edit('edit');
			});

			el.find('.editable .tools .delete-tool').click(function(){
				 
				$('.editable').classList.remove('active');

				$(this).closest('.editable').addClass('active');
				
				tools.delete('delete');
			});

			droppable(el);

			let data = {
				elementid : elementid,
				content : entities.encodeHTML(html)
			}

			let nodes = _self.buildNodes( doc.nodes, active_elementid );

			console.log('el: '+active_elementid);
			console.log( doc.nodes);

			elements.push( data );

			return db.put({
				_id : doc._id,
				_rev : doc._rev,
				css : [],
				js : [],
				templateid : doc.templateid,
				nodes : nodes,

				elements : elements
			});
			

		}).catch(function(err){
			console.log(err);
			console.log('Unable to retrieve template '+template_id);
		});
	},
	buildNodes(n,e){
		let results;

		if( e && n.length > 0 ){
			var test = function(els) {
				const newEls = [];
				for (var i = 0; i < els.length; i++) {
					var newel = test(els[i].elements);
				    if ( els[i].elementid == e ) {
				    	console.log('found');
				    	console.log(newel);
				    	newel.push({ elementid : e, elements : [] });

				      	newEls.push({
				        	elementid : els[i].elementid,
				        	elements : newel
				        });
				    }else{
				    	console.log('not found');
				    	newEls.push({
				        	elementid : els[i].elementid,
				        	elements : newel
				        });
				    }
				}
			  	return newEls;
			}
			results = test(n);
		}else{
			results = [{ elementid : e, elements : [] }];
		}

		console.log(results);

		return results;
	}
}