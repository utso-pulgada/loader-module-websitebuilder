import $ from './external/jquery/jquery.js';
import toolbox from './toolbox.js'; // -- tools module e.g. edit, delete

export default function( obj ){
	obj.droppable({
	  greedy : true,
      drop: function( event, ui ) {
        $(this).addClass( "ui-state-highlight" );

        let _this = $(this);

        setTimeout(function(){
        	_this.removeClass( "ui-state-highlight" );
        },500);

        ui.draggable.removeAttr('style');

        $('.droppable.active').removeClass('active')

        $(this).addClass('active');

        toolbox.tool( ui.draggable.attr('data-id') );
      }
    });

    obj.find('.droppable').droppable({
      greedy : true,
      drop: function( event, ui ) {
        $(this).addClass( "ui-state-highlight" );

        let _this = $(this);

        setTimeout(function(){
        	_this.removeClass( "ui-state-highlight" );
        },500);

        ui.draggable.removeAttr('style');

        $('.droppable.active').removeClass('active')

        $(this).addClass('active');

        toolbox.tool( ui.draggable.attr('data-id') );
      }
    });
	
}