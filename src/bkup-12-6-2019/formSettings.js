import elementsParser from './elementsParser.js';


export default{

	templateOptions(){
		const _self = this;

		let data = [
			{ 
				id : 'custom-id',
				label : 'ID',
				type : 'input',
				target : 'id',
				value : '',
				placeholder : 'Custom ID...',
				wrapper : '<div class="col-12 col-md-4" style="margin-bottom:8px">',

			},
			{ 
				id : 'custom-class',
				label : 'Classes',
				type : 'input',
				target : 'class',
				value : '',
				placeholder : 'Custom classes...',
				wrapper : '<div class="col-12 col-md-4" style="margin-bottom:8px">',
			},
			{ 
				id : 'custom-type',
				label : 'Type',
				type : 'select',
				target : 'element type',
				value : 'section',
				wrapper : '<div class="col-12 col-md-4" style="margin-bottom:8px">',
				options : [
					{
						value : 'section',
						text : 'Section',
						selected : true,
					},
					{
						value : 'div',
						text : 'Div',
						selected : false
					}
				]
			},
			{ 
				id : 'custom-width-input',
				label : 'Width (px/%)',
				type : 'input',
				target : 'width',
				value : '',
				placeholder : 'Custom width...',
				wrapper : '<div class="col-12 col-md-12" id="cutom-width-toggle1" style="margin-bottom:8px" style="display-none">',
			},
			{ 
				id : 'custom-width',
				label : 'Container width',
				type : 'radio',
				target : 'container',
				wrapper : '<div class="col-12 col-md-6" style="margin-bottom:8px">',
				options : [
					{ 
						text : 'Full',
						value : 'full',
						wrapper : '<div class="col-12 col-md-3" style="margin-bottom:8px">',
						selected : true
					},
					{ 
						text : 'Fixed',
						value : 'fixed',
						wrapper : '<div class="col-12 col-md-3" style="margin-bottom:8px">',
						selected : false
					}
				],
			},
			{ 
				id : 'custom-CSS',
				label : 'Custom CSS',
				type : 'textarea',
				target : 'css',
				value : '',
				placeholder : 'Custom css...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
			}

		];

		elementsParser.formSettings(data);

		document.querySelector('#editor-modal .form-settings .btn-submit').addEventListener('click',function(){
			if( _self.saveSettings(data) ){
				modal(false);
			}
		});

	},
	elementOptions(){
		let data = [
			{ 
				id : 'custom-id',
				label : 'ID',
				type : 'input',
				target : 'id',
				value : '',
				placeholder : 'Custom ID...',
				wrapper : '<div class="col-12 col-md-4" style="margin-bottom:8px">',
			},
			{ 
				id : 'custom-class',
				label : 'Classes',
				type : 'input',
				target : 'class',
				value : '',
				placeholder : 'Custom classes...',
				wrapper : '<div class="col-12 col-md-4" style="margin-bottom:8px">',
			},
			{ 
				id : 'custom-width',
				label : 'Width (px/%)',
				type : 'input',
				target : 'width',
				value : '',
				placeholder : 'Custom width...',
				wrapper : '<div class="col-12 col-md-4" style="margin-bottom:8px">',
			},
			{ 
				id : 'custom-CSS',
				label : 'Custom CSS',
				type : 'textarea',
				target : 'css',
				value : '',
				placeholder : 'Custom css...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
			}

		];

		elementsParser.formSettings(data);

		document.querySelector('#editor-modal .form-settings .btn-submit').addEventListener('click',function(){
			if( _self.saveSettings(data) ){
				modal(false);
			}
		});
	},
	saveSettings(data){
		data.forEach(function(el){

			let targets = [
				{

					name : 'id',
					_trigger(e,v){
						e.setAttribute( 'id', '#'+v);
					}
				},
				{

					name : 'class',
					_trigger(e,v){
						let _classes = v.split(' ');
						e.classList.add(...v);
					}
				},
				{

					name : 'width',
					_trigger(e,v){
						e.style.widwth = v;
					}
				},
				{

					name : 'container',
					_trigger(e,v){
						if( v === 'full' ){
							e.querySelector('.element-container').classList.remove('container');
							e.querySelector('.element-container').classList.add('container-fluid');
						}else if( v === 'fixed' ){
							e.querySelector('.element-container').classList.remove('container-fluid');
							e.querySelector('.element-container').classList.add('container');
						}
					}
				}
			];

			if( targets.findIndex( i => i.name === el.target ) != -1 ){

				if( document.querySelector('.editable.active') ){

					let v = ( document.querySelector('#editor-modal .form-settings .'+el.id) ) ? document.querySelector('#editor-modal .form-settings .'+el.id).value : '';

					if( el.type == 'radio' ){
						document.querySelectorAll('#editor-modal .form-settings .'+el.id).forEach(function(item){
							if( this.getAttribute('checked') ){
								v = item.value;
							}
						})
						
					}

					targets.find( i => i.name === el.target )._trigger(document.querySelector('.editable.active'),v)
				}

			}

		});
	}
}