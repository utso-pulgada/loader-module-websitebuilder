export default function(t){
	let errors = [
		{ id : 0, msg : 'Unable to create template' }
	];

	return ( errors.findIndex( i => i.id == t ) != -1 ) ? errors.find( i => i.id === t ).msg : false;
}