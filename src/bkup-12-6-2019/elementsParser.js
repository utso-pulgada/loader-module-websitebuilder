import PouchDB from 'pouchdb-browser';
import elements from './elements.js'; // -- elements interface
import tools from './tools.js'; // -- tools module e.g. edit, delete
import makeid from './makeid.js';

const entities = require("entities");

let db = new PouchDB('websiteeditor');

export default{
	formSettings(t,d){
		let html = '';
		let arr = [ '<div', '<span', '<section', '<label' ];
		d.forEach(function(el){
			let attr = ( el.attr ) ? ' '+el.attr : '';
			if( el.type == 'input' ){
				html+el.wrapper;
				html+='<label>'+el.label+'</label>';
				html+='<input type="input" value="'+el.value+'" placeholder="'+el.placeholder+'" class="form-control '+el.id+'"'+attr+'>';
			}else if( el.type == 'select' ){
				html+el.wrapper;
				html+='<label>'+el.label+'</label>';
				html+='<select value="'+el.value+'" class="form-control '+el.id+'"'+attr+'>';
				el.options.forEach(function(item){
					let selected = ( item.selected ) ? +' selected' : '';
					html+='<option value="'+item.value+'"'+selected+'>'+item.text+'</option>';
				});
				html+='</select>';
			}else if( el.type == 'radio-options' ){
				html+el.wrapper;
				html+='<label>'+el.label+'</label>';

				html+='<div>';
				el.options.forEach(function(item){
					let attr = ( el.attr ) ? ' '+el.attr : '';
					let selected = ( item.selected ) ? +' checked' : '';
					html+=item.wrapper;
					html+='<input type="radio" class="'+el.id+'" name="'+el.id.replace(/-/g,'_')+'" value="'+item.value+'"'+selected+''+attr+'>';
					html+='<label>'+item.text+'</label>';
					for( var i =0; i < arr.length; i++ ){
						if( item.wrapper.indexOf(arr[i]) != -1 ){
							html+=arr[i].replace('<','</')+'>';
						}
					}
				});
				html+='</div>';
			}else if( el.type == 'textarea' ){
				html+el.wrapper;
				html+='<label>'+el.label+'</label>';
				html+='<textarea placeholder="'+el.placeholder+'" class="form-control '+el.id+'"'+attr+'>'+el.value+'</textarea>';
			}

			for( var i =0; i < arr.length; i++ ){
				if( el.wrapper.indexOf(arr[i]) != -1 ){
					html+=arr[i].replace('<','</')+'>';
				}
			}

			if( el.onchange && typeof el.onchange == 'function' ){
				el.onchange();
			}
			if( el.onfocus && typeof el.onfocus == 'function' ){
				el.onfocus();
			}
			if( el.onclick && typeof el.onclick == 'function' ){
				el.onclick();
			}
			if( el.onunfocus && typeof el.onunfocus == 'function' ){
				el.onunfocus();
			}

		});

	},
	sidebarAccordions(){

		let groups = [];

		elements.toolList().forEach(function(el){

			if( groups.findIndex( i => i.name === el.group) == -1 ){
				groups.push({
					name : el.group,
					elements : [el]
				});
			}else{
				groups[ groups.findIndex( i => i.name === el.group) ].elements.push(el);
			}
		});


		let html = '';
		groups.forEach(function(el){

			let accordion_body = '';

			el.elements.forEach(function(el2){
				accordion_body+=`<div class="tool draggable" data-id="${el2.id}">${el2.label}</div>`;
			});

			html+=`
			<div class="accordion">
			<div class="accordion-title">${el.name}</div>
			<div class="accordion-body">${accordion_body}</div>
			</div>`;
		});

		return html;
	}
}