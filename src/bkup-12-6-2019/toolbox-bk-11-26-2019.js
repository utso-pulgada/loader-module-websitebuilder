import PouchDB from 'pouchdb-browser';
import elements from './elements.js'; // -- html interface
import errors from './errors.js' // -- error messages interface
import makeid from './makeid.js' // -- create random hash module
import modal from './modal.js'; // -- modal module

let db = new PouchDB('websiteeditor');

export default{
	tool(t,el){
		switch( t ){
			case 'column':
				this.column(el);
			break;
		}
	},
	column(el){

		db.get(el.getAttribute('data-templateid')).then(function(doc){

			let id = 'element-'+makeid(6)+'-'+doc.elements.length;
			let data = {
				_id : id,
				elementid : id,
				type : 'column',
				attr : {
					id : '',
					class : '',
					style : '',
				},
				contents : '',
				elements : [ // add initial col items
					{
						_id : 'element-'+makeid(6)+'-2',
						elementid : 'element-'+makeid(6)+'-2',
						type : 'column-item',
						attr : {
							id : '',
							class : '',
							style : '',
						},
						contents : '',
						elements : []
					},
					{
						_id : 'element-'+makeid(6)+'-2',
						elementid : 'element-'+makeid(6)+'-2',
						type : 'column-item',
						attr : {
							id : '',
							class : '',
							style : '',
						},
						contents : '',
						elements : []
					}
				]
			};

			let data2 = doc.elements;
			data2.push(data);


			return db.put({
				_id : doc._id,
				_rev : doc._rev,
				templateid : doc._id,
				elements : data2
			}).then(function(){
				elements.column(data,el);
			}).catch(function(err){
				console.log(err);
				modal(true,'Unable to initialize column element');
			});

		}).catch(function(err){
			console.log(err);
			console.log('error retrieving template data');
		});

	}
}