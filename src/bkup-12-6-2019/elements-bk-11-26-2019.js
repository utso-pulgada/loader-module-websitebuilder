import tool from './tool.js'; // -- tool module, edit, add

export default{
	column(t,el){
		let attr = t.attr;
		let _class = ( attr.class != '' ) ? ' class="'+attr.class+'"' : '';
		let _id = ( attr.id != '' ) ? ' id="'+attr.id+'"' : '';
		let _style = ( attr.style != '' ) ? ' style="'+attr.style+'"' : '';
		let edit_tool = this.edit_tool();
		let columns = '';
		t.elements.forEach(function(item){
			let attr = item.attr;
			let _class = ( attr.class != '' ) ? ' class="element-column-item col-12 col-md-6 editable'+attr.class+'"' : ' class="element-column-item col-12 col-md-6 editable"';
			let _id = ( attr.id != '' ) ? ' id="'+attr.id+'"' : '';
			let _style = ( attr.style != '' ) ? ' style="'+attr.style+'"' : '';
			columns+=`<div${_class+_id}${_style} data-elementid="${item.elementid}">${edit_tool}<div class="wrapper" contenteditable="true"></div></div>`;
		});

		let newel = document.createElement('div');
			newel.classList.add('element-column','element-wrapper','row');
			if( _class != '' ){
				newel.setAttribute('class',_class);
			}
			if( _id != '' ){
				newel.setAttribute('id',_id);
			}
			if( _style != '' ){
				newel.setAttribute('id',_style);
			}
			newel.setAttribute('data-elementid',t.elementid);
			newel.innerHTML = columns;
			newel.querySelectorAll('.edit-tool').forEach(function(el2){
				el2.addEventListener('click',function(){
					if( document.querySelector('.editable.active') ){
						document.querySelector('.editable').classList.remove('active');
					}
					this.closest('.editable').classList.add('active');
					tool('add');
				});
			})
			newel.querySelectorAll('.delete-tool').forEach(function(el2){
				el2.addEventListener('click',function(){
					if( document.querySelector('.editable.active') ){
						document.querySelector('.editable').classList.remove('active');
					}
					this.closest('.editable').classList.add('active');
					tool('delete');
				});
			});

		el.appendChild(newel);
	},
	edit_tool(){
		return '<a class="edit-tool" href="#"><i class="ti-pencil"></i></a><a class="delete-tool" href="#"><i class="ti-trash"></i></a>';
	}
}