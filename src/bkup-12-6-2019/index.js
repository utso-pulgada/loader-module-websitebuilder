import PouchDB from 'pouchdb-browser';
import toolbox from './toolbox.js'; // -- toolbox module
import errors from './errors.js'; // -- error messages interface
import makeid from './makeid.js'; // -- create random hash module
import modal from './modal.js'; // -- modal module

let db = new PouchDB('websiteeditor');

(function(){

	const template_editor = {
		init(){ // -- initialize
			const _self = this;


			document.addEventListener('mousedown',function(){
				
				if( ( event.target.classList.contains('editor-tool') || event.target.classList.contains('editor-toolbox') || event.target.classList.contains('editor-tool-wrapper') ) ||  event.target.closest('.editor-tool') ){

				}else{
					if( document.querySelector('.editor-tool.active') ){
						document.querySelector('.editor-tool.active').classList.remove('active');
					}
				}

			});

			let el = document.createElement('div');
				el.classList.add('editor-wrapper');
				el.innerHTML = '<div class="editor-body droppable"></div>';


			document.querySelector('#template-editor').appendChild(el);

			toolbox.templateDroppable();

			_self.createTemplate();

				
			document.addEventListener('touchstart',function(){
				
				if( ( event.target.classList.contains('editor-tool') || event.target.classList.contains('editor-toolbox') || event.target.classList.contains('editor-tool-wrapper') ) ||  event.target.closest('.editor-tool') ){

				}else{
					if( document.querySelector('.editor-tool.active') ){
						document.querySelector('.editor-tool.active').classList.remove('active');
					}
				}

			});

			toolbox.toolSidebar();
		},
		toolbox(t,el){ // -- editor toolbox
			toolbox.tool(t,el);

			if( document.querySelector('.editor-tool.active') ){
				document.querySelector('.editor-tool.active').classList.remove('active');
			}
		},
		emptydiv(c){ // empty all child elements to the given element (c)
	        if( c == null ){
	            return;
	        }

	        while (c.firstChild) {
	            c.removeChild(c.firstChild);
	        }
	    },
		createTemplate(){ // -- create template
			const _self = this;

			db.allDocs({
				include_docs : true
			}).then(function(docs){	
				let id = 'template-'+makeid(5)+'-'+docs.total_rows;

				db.put({ 
					_id : id,
					templateid : id,
					nodes : [],
					elements : []
				}).then(function(){

					document.querySelector('#template-editor').setAttribute('data-templateid',id);
					document.querySelector('#template-editor').classList.add('active');

				}).catch(function(err){
					console.log(err);
					console.log('Unable to create new template');
					
					// -- 
					if( errors(0) ){
						let contents = '<div style="margin-bottom:16px;">'+errors(0)+'</div><button class="btn btn-primary">Try again</button>';
						modal(true,contents);

						// append a try again button to the modal
					    let trybtn = document.createElement('button');
					    	trybtn.classList.add('btn','btn-primary','tryagain-btn');
					    	trybtn.textContent = 'Try Again'
					    	trybtn.addEventListener('click',function(){
					    		_self.createElement();
					    	});

					   	if( !document.querySelector('#template-editor-modal .editor-modal-contents .tryagain-btn') ){
					   		document.querySelector('#template-editor-modal .editor-modal-contents').appendChild(trybtn);
					 	 }


					}else{
						console.log('Error message does not match');
					}

				});
			}).catch(function(err){
				console.log(err);
				console.log('unable to fetch all templates');
			});
		}
	}

	template_editor.init();
})();