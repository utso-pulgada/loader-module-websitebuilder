import modal from './modal.js'; // -- modal module

export default{
	add( t ){

	},
	delete(){
		modal(true,'<div style="margin-bottom:16px;">Are you sure you want to delete this element? click \'Delete\' to proceed</div><button class="btn btn-danger delete">Delete</button>');

		document.querySelector('#editor-modal .delete').addEventListener('click',function(){
			if( document.querySelector('.editable.active') ){
				document.querySelector('.editable.active').remove();

			}
			modal(false);
		});
	},

}