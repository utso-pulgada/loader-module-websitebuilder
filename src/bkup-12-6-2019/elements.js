export default{
	column(c,id){
		let html = '<div class="row">';
		for( var i = 0; i < c; i++ ){
			html+=`<div class="droppable element-column-item col-12 col-md-6 editable">edit_tool<div class="wrapper editable-item"></div></div>`;
		}
		html+'</div>'

		return html;
	},
	tools(t){
		if( t == 'delete' ){
			return '<a class="delete-tool with-tools" href="#"><i class="ti-trash"></i></a>';
		}
		if( t == 'edit' ){
			return '<a class="edit-tool" href="#"><i class="ti-pencil"></i></a>';
		}
	},
	sidebarAccordions(){
		return `
		<div class="accordion">
		<div class="accordion-title"><h5>Containers, Wrappers</h5></div>
		<div class="accordion-body">
        ${this.toolList()}
		</div>
		</div>
		`;
	},
	toolList(){
		return [{
				id : 'element_section',
				label : 'Section',
				contents : '<section class="element droppable element-section edit-tool delete-tool with-tools"><div class="container-fluid droppable"></div></section>',
				group : 'wrapper',
				options : ['id','class','custom-width','width','css'],
			},
			{
				id : 'element_div',
				label : 'Div',
				contents : '<div class="element droppable element-div editable edit-tool delete-tool with-tools"></div>',
				group : 'wrapper',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_paragraph',
				label : 'Paragraph',
				contents : '<p class="element element-paragraph editable edit-tool delete-tool with-tools"></p>',
				group : 'elements',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_table',
				label : 'Table',
				contents : '<div class="element element-table edit-tool delete-tool with-tools"><table class="table"><thead><tr><td class="editable">Header 1</td><td class="editable">Header 2</td></tr></thead><tbody><tr><td class="editable"></td><td class="editable"></td></tr></tbody></table></div>',
				group : 'components',
				options : ['id','class','attr','css','table'],
				html_template : true
			},
			{
				id : 'element_column',
				label : 'Column',
				contents : '<div class="element element-column edit-tool delete-tool row with-tools"><div class="col-12 col-md-6 droppable"></div><div class="col-12 col-md-6 droppable"></div></div>',
				group : 'components',
			 	options : ['id','class','css','col','attr'],
			},
			{
				id : 'element_image',
				label : 'Image',
				contents : '<img src="" alt="Image" width="" height="" class="element element-image edit-tool delete-tool with-tools">',
				group : 'elements',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_header1',
				label : 'H1',
				contents : '<h1 class="element element-header1 editable delete-tool edit-tool with-tools"></h1>',
				group : 'header',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_header2',
				label : 'H2',
				contents : '<h2 class="element element-header2 editable delete-tool edit-tool with-tools"></h2>',
				group : 'header',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_header3',
				label : 'H3',
				contents : '<h3 class="element element-header3 editable delete-tool edit-tool with-tools"></h3>',
				group : 'header',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_header4',
				label : 'H4',
				contents : '<h4 class="element element-header4 editable delete-tool edit-tool with-tools"></h4>',
				group : 'header',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_header5',
				label : 'H5',
				contents : '<h5 class="element element-header5 editable delete-tool edit-tool with-tools"></h5>',
				group : 'header',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'element_header6',
				label : 'H6',
				contents : '<h6 class="element element-header6 editable delete-tool edit-tool with-tools"></h6>',
				group : 'header',
				options : ['id','class','attr','css'],
				html_template : true
			},
			{
				id : 'elements-html',
				label : 'HTML',
				contents : '<div class="element element-html edit-tool delete-tool with-tools"></div>',
				group : 'components',
				options : ['html'],
				html_template : true
			},
			{
				id : 'elements-form',
				label : 'Form',
				contents : '<form action="" class="droppable element element-form editable delete-tool edit-tool with-tools" method="post"></form>',
				group : 'form',
				options : ['form','attr','id','class','css'],
			},
			{
				id : 'elements-input',
				label : 'Input',
				contents : '<input type="text" class="element element-input form-control delete-tool edit-tool with-tools" name="" placeholder="" value="">',
				group : 'form',
				options : ['input','attr','id','class','css'],
			},
			{
				id : 'elements-checkbox',
				label : 'Checkbox',
				contents : '<input type="checkbox" class="element element-checkbox delete-tool edit-tool with-tools" name="" value="">',
				group : 'form',
				options : ['input','attr','id','class','css'],
			},
			{
				id : 'elements-radio',
				label : 'Radio',
				contents : '<input type="radio" class="element element-radio delete-tool edit-tool with-tools" name="" value="">',
				group : 'form',
				options : ['input','attr','id','class','css'],
			},
			{
				id : 'elements-select',
				label : 'Select',
				contents : '<select class="element element-select form-control delete-tool edit-tool with-tools"></select>',
				group : 'form',
				options : ['select','attr','id','class','css'],
			},
			{
				id : 'elements-button',
				label : 'Button',
				contents : '<button class="element element-button btn btn-primay edit-tool delete-tool with-tools">Text</button>',
				group : 'form',
				options : ['select','attr','id','class','css'],
			}
		];
	}
}