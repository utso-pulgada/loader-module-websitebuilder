export default function(t=false,c=''){ // -- editor modal
	// <t> true/false, true = open, false = close
	// <c> contents
	// create modal if not created yet
	if( !document.querySelector('#editor-modal') ){
		let el = document.createElement('div');
			el.setAttribute('id','editor-modal');
			el.classList.add('editor-modal');
			el.innerHTML = '<div id="editor-modal-body" class="editor-modal-body wb-main-wrapper"><a href="#" class="editor-close-btn" id="editor-close-btn"><i class="ti-close"></i></a><div class="editor-modal-contents">'+c+'</div></div>';
			el.querySelector('#editor-close-btn').onclick = function(){
				this.closest('#editor-modal').classList.remove('active');
				if( document.querySelector('.with-tools.tool-active') ){
					document.querySelector('.with-tools.tool-active').classList.remove('tool-active');
					document.querySelector('#editor-modal').setAttribute('data-settingsid','default');
				}
			}
			document.querySelector('body').appendChild(el);
	}else{
		document.querySelector('#editor-modal .editor-modal-contents').innerHTML = c;
	}
	if( t ){
		document.querySelector('#editor-modal').classList.add('active');
	}else{
		document.querySelector('#editor-modal').classList.remove('active');

		document.querySelectorAll('#template-editor .editor-body .tool-active').forEach(function(item){
			item.classList.remove('tool-active')
		});
		document.querySelectorAll('.droppable').forEach(function(item){
			item.classList.remove('active')
		});
		document.querySelectorAll('.text-active').forEach(function(item){
			item.classList.remove('text-active')
		});
	}
}