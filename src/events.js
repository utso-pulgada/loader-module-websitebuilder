import $ from './jqueryui/jquery.js';
import elementsParser from './elementsParser.js'; // -- elements parser
import elements from './elements.js'; // -- html interface
import modal from './modal.js';

export default{
	toolEvents(t){
		const _self = this;
		$(t).find('.with-tools').each(function(){
			$(this).find('.tools').remove();

			let tools = '<div class="tools"><div>';
			if( $(this).hasClass('edit-tool') ){
				tools+=elements.tools('edit');;
			}

			if( $(this).hasClass('delete-tool') ){
				tools+=elements.tools('delete');
			}
			tools+='</div></div>';

			$(this).prepend(tools);

			$(this).find('.tools .edit-tool').click(function(){
		 
				$('.droppable').removeClass('active');
				$('.with-tools.tool-active').removeClass('tool-active');

				$(this).closest('.with-tools').addClass('tool-active');

				let formsettings = elements.toolList().find( i => i.id == $(this).closest('.with-tools').attr('data-elementtype') ).settings;

				elementsParser.settings( formsettings );
				
				setTimeout(function(){
					formsettings.forEach(function(item){
						elements.settingsOptions(item).onedit();
					});
				},100);

				document.querySelector('#editor-modal').setAttribute('data-settingsid','default');
			});		

			$(this).find('.tools .delete-tool').click(function(){
				 
				$('.with-tools.tool-active').removeClass('tool-active');

				$(this).closest('.with-tools').addClass('tool-active');
				
				modal(true,'<div style="margin-bottom:16px;">Are you sure you want to delete this element? click \'Delete\' to proceed</div><button class="btn btn-danger delete">Delete</button>');

				document.querySelector('#editor-modal .delete').addEventListener('click',function(){
					if( document.querySelector('#template-editor .with-tools.tool-active') ){
						// -- update the template database
						
						document.querySelector('#template-editor .with-tools.tool-active').remove(); // remove
					}

					modal(false);

				});

			});
			

		});
	}
}