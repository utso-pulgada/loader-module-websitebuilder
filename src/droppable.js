import $ from './jqueryui/jquery.js';
import './jqueryui/jquery-ui.min.css';
import './jqueryui/jquery-ui.min.js';
import toolbox from './toolbox.js'; // -- tools module e.g. edit, delete

export default function( obj ){
	$(obj).droppable({
	   greedy : true,
      hoverClass: "drop-hover",
      cancel: '.tools',
      drop: function( event, ui ) {
        $('.droppable.active').removeClass('active');
        $(this).addClass( "ui-state-highlight" );
        $(this).addClass('active');

        if( ui.draggable.hasClass('element') ){

          ui.draggable.appendTo($(this));

        }else if( ui.draggable.closest('.element').length > 0 ){
          
          ui.draggable.closest('.element').appendTo($(this));

        }else{
         
          ui.draggable.removeAttr('style');
          toolbox.tool( ui.draggable.attr('data-id') );

        }

        let _this = $(this);

        setTimeout(function(){
          _this.removeClass( "ui-state-highlight drop-hover" );
        },500);

      },
    });

    $(obj).find('.droppable').droppable({
      greedy : true,
      hoverClass: "drop-hover",
      cancel: '.tools',
      drop: function( event, ui ) {

        $('.droppable.active').removeClass('active')
        $(this).addClass( "ui-state-highlight" );
        $(this).addClass('active');

        if( ui.draggable.hasClass('element') ){
          
          ui.draggable.appendTo($(this));          

        }else if( ui.draggable.closest('.element').length > 0 ){
          
          ui.draggable.closest('.element').appendTo($(this));

        }else{  
          
          ui.draggable.removeAttr('style');
          toolbox.tool( ui.draggable.attr('data-id') );
        }
        
        let _this = $(this);

        setTimeout(function(){
          _this.removeClass( "ui-state-highlight drop-hover" );
        },500);

      }
    });
	
}