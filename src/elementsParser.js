import elements from './elements.js'; // -- elements interface
import modal from './modal.js';
import tab from './tab.js';
import toolEvents from './events.js';
import droppable from './droppable.js';

export default{
	saveSettings(){
		document.querySelectorAll('#editor-modal .setting').forEach(function(el){
			elements.settingsOptions(el.getAttribute('data-settingsid')).save();
			toolEvents.toolEvents(document.querySelector('.with-tools.tool-active').closest('.droppable'));
			toolEvents.toolEvents(document.querySelector('.with-tools.tool-active'));
			droppable(document.querySelector('.with-tools.tool-active').closest('.element').querySelectorAll('.droppable'));
		});	
		// -- modal off
		setTimeout(function(){
			modal(false);
		},300);

	},
	cols_layout(){
		return `<div class="row">
				<div class="col-12 col-md-6" style="margin-bottom:8px">
				<label>Width default</label><br>
				<select class="form-control width-default">
				<option value="col-lg-12">column 12 - 100%</option>
				<option value="col-lg-11">column 11 - 91%</option>
				<option value="col-lg-10">column 10 - 83%</option>
				<option value="col-lg-9">column 9 - 75%</option>
				<option value="col-lg-8">column 8 - 66%</option>
				<option value="col-lg-7">column 7 - 58%</option>
				<option value="col-lg-6">column 6 - 50%</option>
				<option value="col-lg-5">column 5 - 41%</option>
				<option value="col-lg-4">column 4 - 33%</option>
				<option value="col-lg-3">column 3 - 25%</option>
				<option value="col-lg-2">column 2 - 16%</option>
				<option value="col-lg-1">column 1 - 8%</option>
				</select>
				</div>
				<div class="col-12 col-md-6" style="margin-bottom:8px">
				<label>Width tablet</label><br>
				<select class="form-control width-tablet">
				<option value="col-md-12">column 12 - 100%</option>
				<option value="col-md-11">column 11 - 91%</option>
				<option value="col-md-10">column 10 - 83%</option>
				<option value="col-md-9">column 9 - 75%</option>
				<option value="col-md-8">column 8 - 66%</option>
				<option value="col-md-7">column 7 - 58%</option>
				<option value="col-md-6">column 6 - 50%</option>
				<option value="col-md-5">column 5 - 41%</option>
				<option value="col-md-4">column 4 - 33%</option>
				<option value="col-md-3">column 3 - 25%</option>
				<option value="col-md-2">column 2 - 16%</option>
				<option value="col-md-1">column 1 - 8%</option>
				</select>
				</div>
				<div class="col-12 col-md-6" style="margin-bottom:8px">
				<label>Width tablet small</label><br>
				<select class="form-control width-tablet-small">
				<option value="col-sm-12">column 12 - 100%</option>
				<option value="col-sm-11">column 11 - 91%</option>
				<option value="col-sm-10">column 10 - 83%</option>
				<option value="col-sm-9">column 9 - 75%</option>
				<option value="col-sm-8">column 8 - 66%</option>
				<option value="col-sm-7">column 7 - 58%</option>
				<option value="col-sm-6">column 6 - 50%</option>
				<option value="col-sm-5">column 5 - 41%</option>
				<option value="col-sm-4">column 4 - 33%</option>
				<option value="col-sm-3">column 3 - 25%</option>
				<option value="col-sm-2">column 2 - 16%</option>
				<option value="col-sm-1">column 1 - 8%</option>
				</select>
				</div>
				<div class="col-12 col-md-6" style="margin-bottom:8px">
				<label>Width phone</label><br>
				<select class="form-control width-phone">
				<option value="col-12">column 12 - 100%</option>
				<option value="col-11">column 11 - 91%</option>
				<option value="col-10">column 10 - 83%</option>
				<option value="col-9">column 9 - 75%</option>
				<option value="col-8">column 8 - 66%</option>
				<option value="col-7">column 7 - 58%</option>
				<option value="col-6">column 6 - 50%</option>
				<option value="col-5">column 5 - 41%</option>
				<option value="col-4">column 4 - 33%</option>
				<option value="col-3">column 3 - 25%</option>
				<option value="col-2">column 2 - 16%</option>
				<option value="col-1">column 1 - 8%</option>
				</select>
				</div>
				</div>`;
	},
	settings(data){

		const _self = this;		

		let new_arr = [];

		data.forEach(function(el){
			if( elements.settingsOptions(el) ){
				new_arr.push(elements.settingsOptions(el));
			}
		});

		let html = '<div class="row">';

		let arr = [ '<div', '<span', '<section', '<label' ];

		new_arr.forEach(function(el){
			let attr = ( el.attr ) ? ' '+el.attr : '';
			html+=el.wrapper;
			if( el.type == 'input' || el.type == 'number' ){
				
				html+='<label>'+el.label+'</label>';
				if( el.type == 'number' ){

					let min = ( 'min' in el ) ? ' min="'+el.min+'"' : '';
					let max = ( 'max' in el ) ? ' max="'+el.max+'"' : ''; 

					attr+=min;
					attr+=max;
				}

				html+='<input type="'+( (el.type == 'number') ? 'number' : 'text' )+'" value="'+el.value+'" data-settingsid="'+el.id+'" placeholder="'+el.placeholder+'" class="setting form-control"'+attr+'>';

			}else if( el.type == 'select' ){
				html+='<label>'+el.label+'</label>';
				html+='<select value="'+el.value+'" data-settingsid="'+el.id+'" class="setting form-control"'+attr+'>';
				el.options.forEach(function(item){
					let selected = ( item.selected ) ? +' selected' : '';
					html+='<option value="'+item.value+'"'+selected+'>'+item.text+'</option>';
				});
				html+='</select>';
			}else if( el.type == 'checkbox' ){
				html+='<label>'+el.label+'</label>';

				html+='<div class="row setting" data-settingsid="'+el.id+'" >';
				let attr = ( el.attr ) ? ' '+el.attr : '';
				let selected = ( typeof el.selected != 'undefined' && el.selected ) ? ' checked' : '';
				html+=el.wrapper;
				html+='<input type="checkbox" name="'+el.id.replace(/-/g,'_')+'" value="'+el.value+'"'+selected+''+attr+'>';
				html+='<label style="margin-left:5px">'+el.text+'</label>';
				for( var i =0; i < arr.length; i++ ){
					if( el.wrapper.indexOf(arr[i]) != -1 ){
						html+=arr[i].replace('<','</')+'>';
						break;
					}
				}
				html+='</div>';
			}else if( el.type == 'checkbox-options' ){
				html+='<label>'+el.label+'</label>';

				html+='<div class="row setting" data-settingsid="'+el.id+'" >';
				el.options.forEach(function(item){
					let attr = ( el.attr ) ? ' '+el.attr : '';
					let selected = ( item.selected ) ? ' checked' : '';
					html+=item.wrapper;
					html+='<input type="checkbox" name="'+item.name.replace(/-/g,'_')+'" value="'+item.value+'"'+selected+''+attr+'>';
					html+='<label style="margin-left:5px">'+item.text+'</label>';
					for( var i =0; i < arr.length; i++ ){
						if( item.wrapper.indexOf(arr[i]) != -1 ){
							html+=arr[i].replace('<','</')+'>';
							break;
						}
					}
				});
				html+='</div>';
			}else if( el.type == 'radio-options' ){
				html+='<label>'+el.label+'</label>';

				html+='<div class="row setting" data-settingsid="'+el.id+'" >';
				el.options.forEach(function(item){
					let attr = ( el.attr ) ? ' '+el.attr : '';
					let selected = ( item.selected ) ? ' checked' : '';
					html+=item.wrapper;
					html+='<input type="radio" name="'+el.id.replace(/-/g,'_')+'" value="'+item.value+'"'+selected+''+attr+'>';
					html+='<label style="margin-left:5px">'+item.text+'</label>';
					for( var i =0; i < arr.length; i++ ){
						if( item.wrapper.indexOf(arr[i]) != -1 ){
							html+=arr[i].replace('<','</')+'>';
							break;
						}
					}
				});
				html+='</div>';
			}else if( el.type == 'textarea' ){
				html+='<label>'+el.label+'</label>';
				html+='<textarea placeholder="'+el.placeholder+'" data-settingsid="'+el.id+'" class="setting form-control"'+attr+'>'+el.value+'</textarea>';
			}else if( el.type == 'option-select' ){
				html+='<label>'+el.label+'</label><br>';
				html+='<button class="btn btn-sm btn-primary add-option" style="margin-bottom:8px;">New option</button>';
				html+='<div class="table-responsive"><table class="table table-bordered setting" data-settingsid="'+el.id+'">';
				html+='<thead><tr><th></th><th>Text</th><th>Value</th><th>Selected?</th></tr></thead>';
				html+='<tbody></tbody>';
				html+='</table></div>';
			}else if( el.type == 'column' ){
				html+='<div class="setting" data-settingsid="'+el.id+'" style="border: 1px solid #ddd;padding:16px;margin-top: 16px;position: relative;">';
				html+='<label>'+el.label+'</label>';
				html+='<label style="position: absolute;top: -11px;background: #FFFF;left: 8px;font-size: 12px;padding: 0px 5px;">Column settings</label>';
				html+='<select class="column-count form-control" style="margin-bottom:16px;"><option value="1">1</option><option value="2" selected>2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option></select>';

				var tabs = [];

				let active_tool = document.querySelector('.tool-active').querySelectorAll('.col-item');

				for( var i = 0;i < active_tool.length; i++ ){

					tabs.push({
						'header' : 'Column '+(i+1),
						'contents' : _self.cols_layout()
					});
				}


				html+=tab.build(tabs); // -- get the tabs layout
				html+='</div>';
			}else if( el.type == 'custom-resources' ){
				html+='<div class="setting" data-settingsid="'+el.id+'">'
				html+=tab.build([
					{
						header : 'Header resources',
						contents : '<ul class="custom-resources custom-resources-header"></ul><input type="text" class="form-control custom-resources-input header" value="" placeholder="Type and enter to add">'

					},
					{
						header : 'Footer resources',
						contents : '<ul class="custom-resources custom-resources-footer"></ul><input type="text" class="form-control custom-resources-input footer" value="" placeholder="Type and enter to add">'

					}
				]);
				html+='</div>';

			}


			for( var i =0; i < arr.length; i++ ){
				if( el.wrapper.indexOf(arr[i]) != -1 ){
					html+=arr[i].replace('<','</')+'>';
					break;
				}
			}

		});
		html+='<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:16px"><button class="btn btn-block btn-primary btn-save-settings">Save</button></div></div>';

		modal(true,html);

		// -- call the tabs function
		if( document.querySelectorAll('#editor-modal .wbtab').length > 0 ){
			tab.init(document.querySelector('#editor-modal .wbtab'));
		}

		setTimeout(function(){
			// -- dynamic column if has one
			if( document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns"] .column-count') ){
				document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns"] .column-count').addEventListener('change',function(){
					if( this.value != '' ){
						if( this.value < document.querySelectorAll('#editor-modal .setting[data-settingsid="columns"] .wbtab .wbtab-contents .wbtab-content').length ){
							let eq2 = document.querySelectorAll('#editor-modal .setting[data-settingsid="columns"] .wbtab .wbtab-contents .wbtab-content').length - this.value;
							for( var i = 0; i < eq2; i++ ){
								tab.removeTab(document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab'));
							}
						}else{
							let eq1 = this.value-( document.querySelectorAll('#editor-modal .setting[data-settingsid="columns"] .wbtab .wbtab-contents .wbtab-content').length );
							for( var i = 0;i<eq1;i++){
								let arr = {
									header : 'Column '+(document.querySelectorAll('#editor-modal .setting[data-settingsid="columns"] .wbtab .wbtab-contents .wbtab-content').length+1),
									contents : _self.cols_layout()
								}
								tab.addTab(document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab'),arr);
								
							}
						}
					}
				});					
			}
			// -- custom resources
			if( document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="custom-resources"]') ){
				document.querySelectorAll('#editor-modal #editor-modal-body .setting[data-settingsid="custom-resources"] .custom-resources-input').forEach(function(item){

					item.addEventListener('keyup',function(){
						if (event.which === 13 || event.keyCode === 13 || event.key === "Enter"){
							let el = document.createElement('li');
							el.setAttribute('style','display:inline-table;margin:3px 16px 3px 0px');
							el.setAttribute('data-link',this.value);
							el.innerHTML = this.value+` <span class="badge badge-danger remove" onclick="this.closest('li').remove()">remove</span>`;
							this.closest('.wbtab-content').querySelector('.custom-resources').appendChild(el);
							this.value = '';
						}
					});
				});
			}
			// -- save settings
			document.querySelector('#editor-modal #editor-modal-body .btn-save-settings').addEventListener('click',function(){
				_self.saveSettings();
			});
		},500);
	},
	sidebarAccordions(){

		let groups = [];

		elements.toolList().forEach(function(el){

			if( groups.findIndex( i => i.name === el.group) == -1 ){
				groups.push({
					name : el.group,
					elements : [el]
				});
			}else{
				groups[ groups.findIndex( i => i.name === el.group) ].elements.push(el);
			}
		});


		let html = '';
		groups.forEach(function(el){

			let accordion_body = '';

			el.elements.forEach(function(el2){
				accordion_body+=`<div class="tool draggable" data-id="${el2.id}">${el2.label}</div>`;
			});

			html+=`
			<div class="accordion">
			<div class="accordion-title">${el.name}</div>
			<div class="accordion-body">${accordion_body}</div>
			</div>`;
		});

		return html;
	}
}