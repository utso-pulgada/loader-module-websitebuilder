import $ from './jqueryui/jquery.js';
import PouchDB from 'pouchdb-browser';
import modal from './modal.js';

const entities = require("entities");
let db = new PouchDB('websiteeditor');

export default function(){
	document.querySelector('#save-template').disabled = true;

	let template_id = $('#template-editor').attr('data-templateid');

	db.get( template_id ).then(function(doc){


		let el = $('<div>').append($('#template-editor .editor-body').clone());

		el.find('.tools').remove();
		el.find('.with-tools.tool-active').removeClass('tool-active');
		el.find('.droppable').removeClass('active');

		let custom_header_ref = [];
		let custom_footer_ref = [];

		document.querySelector('#custom-resources-ref .custom-resources-header li').forEach( function(item) {
			custom_header_ref.push(item.textContent);
		});

		document.querySelector('#custom-resources-ref .custom-resources-footer li').forEach( function(item) {
			custom_footer_ref.push(item.textContent);
		});
		
		return db.put({
			_id : doc._id,
			_rev : doc._rev,
			templateid : doc.templateid,
			template_name : document.querySelector('#template-editor').getAttribute('data-templatename'),
			custom_css : entities.encodeHTML( document.querySelector('#custom-resources-ref #custom-style-ref').innerHTML ),
			custom_js : entities.encodeHTML( document.querySelector('#custom-resources-ref #custom-js-ref').innerHTML ),
			custom_resources_header : custom_header_ref,
			custom_resources_footer :custom_footer_ref, 
			contents : entities.encodeHTML(el.find('.editor-body').html()),
			created_at : doc.created_at,
			updated_at : ( ( new Date() ).getTime() ).toString()
		}).then(function(){
			
			document.querySelector('#save-template').disabled = false;

			$('.with-tools').removeClass('tool-active active');

			console.log('updateElement: successfully updated');

			modal(true,`<span style="
    width: 30px;
    height: 30px;
    border: 1.5px solid #71e100;
    display: flex;
    float: left;
    align-items: center;
    justify-content: center;
    text-align: center;
    margin-right: 8px;
    border-radius: 50%;
"><i class="ti-check color-success" style="color: #71e100;"></i></span>Successfully updated`);

			setTimeout( function(){
				modal(false);
			},1000 );
		}).catch(function(err){
			console.log(err);
			console.log('updateElement: failed');

			modal(true,`<span style="
    width: 30px;
    height: 30px;
    border: 1.5px solid #e32a40;
    display: flex;
    float: left;
    align-items: center;
    justify-content: center;
    text-align: center;
    margin-right: 8px;
    border-radius: 50%;
"><i class="ti-alert color-success" style="color: #71e100;"></i></span>Unable to save template ${template_id}`);
		});
	}).catch(function(err){
		console.log(err);
		console.log('updateElement: Unable to retrieve template '+template_id);

			modal(true,`<span style="
    width: 30px;
    height: 30px;
    border: 1.5px solid #e32a40;
    display: flex;
    float: left;
    align-items: center;
    justify-content: center;
    text-align: center;
    margin-right: 8px;
    border-radius: 50%;
"><i class="ti-alert color-success" style="color: #71e100;"></i></span>Unable to retrieve template ${template_id}`);
	});
}