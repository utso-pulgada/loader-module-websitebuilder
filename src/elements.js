import tinymce from './tinymce/tinymce.js';

export default{
	attachEvents(){

	},
	tools(t){
		if( t == 'delete' ){
			return '<a class="delete-tool" href="#"><i class="ti-trash"></i></a>';
		}
		if( t == 'edit' ){
			return '<a class="edit-tool" href="#"><i class="ti-pencil"></i></a>';
		}
	},
	sidebarAccordions(){
		return `
		<div class="accordion">
		<div class="accordion-title"><h5>Containers, Wrappers</h5></div>
		<div class="accordion-body">
        ${this.toolList()}
		</div>
		</div>
		`;
	},
	toolList(){
		return [
			{
				id : 'element-section',
				label : 'Section',
				contents : '<section class="element droppable element-section edit-tool delete-tool with-tools"></section>',
				element_type : 'section',
				group : 'wrapper',
				settings : ['custom-id','custom-class','custom-css']
			},
			{
				id : 'element-div',
				label : 'Div',
				contents : '<div class="element droppable element-div edit-tool delete-tool with-tools"></div>',
				group : 'wrapper',
				element_type : 'div',
				html_template : true,
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'element-paragraph',
				label : 'Paragraph',
				contents : '<div class="edit-tool delete-tool with-tools element element-paragraph element-wrapper"><p class="height-inherit"></p></div>',
				element_type : 'p',
				group : 'elements',
				html_template : true,
				activate_settings : ['custom-editor'],
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'element-table',
				label : 'Table',
				contents : '<div class="element element-table edit-tool delete-tool with-tools element-wrapper"><table class="table element-item"><thead><tr><th>Header 1</th><th>Header 2</th></tr></thead><tbody><tr><td class="droppable"></td><td class="droppable"></td></tr></tbody></table></div>',
				group : 'components',
				element_type : 'table',
				html_template : true,
				activate_settings : ['columns-table','rows-table','table-header'],
				settings : ['table-header','custom-id','custom-class','width','height','columns-table','rows-table','custom-css'],
			},
			{
				id : 'element-column',
				label : 'Column',
				contents : '<div class="container-fluid container-wrapper"><div class="element element-column with-tools edit-tool delete-tool row element-wrapper"><div class="col-item col-12 col-sm-12 col-md-6 col-lg-6 droppable"></div><div class="col-item col-12 col-sm-12 col-md-6 col-lg-6 droppable"></div></div></div>',
				element_type : 'col-item',
				group : 'components',
			 	activate_settings : ['columns'],
			 	settings : ['custom-id','custom-class','columns','custom-width','custom-css']
			},
			{
				id : 'element-image',
				label : 'Image',
				contents : '<div class="element element-image edit-tool delete-tool with-tools element-wrapper"><img src="" class="element-item" alt="Image" width="" height=""></div>',
				group : 'elements',
				element_type : 'img',
				html_template : true,
				activate_settings : ['img-src','width','height','img-alt','custom-class','custom-id','custom-css'],
				settings : ['img-src','width','height','img-alt','custom-class','custom-id','custom-css']
			},
			{
				id : 'element-header1',
				label : 'H1',
				contents : '<div class="element element-header1 delete-tool edit-tool with-tools element-wrapper"><h1 class="height-inherit" contentEditable="true"></h1></div>',
				group : 'header',
				element_type : 'h1',
				html_template : true,
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'element-header2',
				label : 'H2',
				contents : '<div class="element element-header2 delete-tool edit-tool with-tools element-wrapper"><h2 class="height-inherit" contentEditable="true"></h2></div>',
				element_type : 'h2',
				group : 'header',
				html_template : true,
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'element-header3',
				label : 'H3',
				contents : '<div class="element element-header3 delete-tool edit-tool with-tools element-wrapper"><h3 class="height-inherit" contentEditable="true"></h3></div>',
				element_type : 'h3',
				group : 'header',
				html_template : true,
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'element-header4',
				label : 'H4',
				contents : '<div class="element element-header4 delete-tool edit-tool with-tools element-wrapper"><h4 class="height-inherit" contentEditable="true"></h4</div>',
				element_type : 'h4',
				group : 'header',
				html_template : true,
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'element-header5',
				label : 'H5',
				contents : '<div class="element element-header5 delete-tool edit-tool with-tools element-wrapper"><h5 class="height-inherit" contentEditable="true"></h5></div>',
				element_type : 'h5',
				group : 'header',
				html_template : true,
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'element-header6',
				label : 'H6',
				contents : '<div class="element element-header6 delete-tool edit-tool with-tools element-wrapper"><h6 class="height-inherit" contentEditable="true"></h6></div>',
				element_type : 'h6',
				group : 'header',
				html_template : true,
				settings : ['custom-id','width','height','custom-class','custom-editor','custom-css']
			},
			{
				id : 'elements-html',
				label : 'HTML',
				contents : '<div class="element element-html edit-tool delete-tool with-tools"><div class="html-wrapper"></div></div>',
				element_type : 'html-wrapper',
				group : 'components',
				html_template : true,
				activate_settings : ['custom-html'],
				settings : ['custom-html']
			},
			{
				id : 'elements-form',
				label : 'Form',
				contents : '<form action="" class="droppable element element-form delete-tool edit-tool with-tools" method="post"></form>',
				element_type : 'form',
				group : 'form',
				settings : ['custom-id','width','height','custom-class','form-action','form-method','form-multipart','custom-css']
			},
			{
				id : 'elements-input',
				label : 'Input',
				contents : '<div class="element element-input delete-tool edit-tool with-tools element-wrapper"><input type="text" class="form-control" name="" placeholder="" value=""></div>',
				element_type : 'input',
				group : 'form',
				settings : ['custom-id','width','height','custom-class','custom-css']
			},
			{
				id : 'elements-checkbox',
				label : 'Checkbox',
				contents : '<div class="element element-checkbox delete-tool edit-tool with-tools element-wrapper"><input type="checkbox" class="element-item" name="" value=""></div>',
				element_type : 'input',
				group : 'form',
				settings : ['option-label','custom-id','width','height','custom-class','custom-css']
			},
			{
				id : 'elements-radio',
				label : 'Radio',
				contents : '<div class="element element-radio delete-tool edit-tool with-tools element-wrapper"><input type="radio" class="element-item" name="" value=""></div>',
				element_type : 'input',
				group : 'form',
				settings : ['option-label','custom-id','width','height','custom-class','custom-css']
			},
			{
				id : 'elements-select',
				label : 'Select',
				contents : '<div class="element element-select delete-tool edit-tool with-tools element-wrapper"><select class="form-control"></select></div>',
				element_type : 'select',
				element_type : 'select',
				group : 'form',
				settings : ['custom-id','width','height','custom-class','custom-css']
			},
			{
				id : 'elements-button',
				label : 'Button',
				contents : '<div class="wb-main-wrapper element element-button delete-tool edit-tool with-tools element-wrapper"><button class="btn">Text</button></div>',
				element_type : 'button',
				group : 'form',
				settings : ['custom-id','width','custom-text','height','custom-class','buttons','custom-css']
			},
			{
				id : 'elements-icon',
				label : 'Icon',
				contents : '<div class="wb-main-wrapper element element-button delete-tool edit-tool with-tools element-wrapper" style="display:inline-table;"><i class=""></i></div>',
				element_type : 'i',
				group : 'components',
				activate_settings : ['icon'],
				settings : ['custom-id','width','height','custom-class','icon']
			}
		];
	},
	columns(){
		return [
			{ 
				id : 'desktop',
				cols : [
					'col-lg-12',
					'col-lg-10',
					'col-lg-11',
					'col-lg-9',
					'col-lg-8',
					'col-lg-7',
					'col-lg-6',
					'col-lg-5',
					'col-lg-4',
					'col-lg-3',
					'col-lg-2',
					'col-lg-1'
				]
			},
			{ 
				id : 'tablet',
				cols : [
					'col-md-12',
					'col-md-10',
					'col-md-11',
					'col-md-9',
					'col-md-8',
					'col-md-7',
					'col-md-6',
					'col-md-5',
					'col-md-4',
					'col-md-3',
					'col-md-2',
					'col-md-1'
				]
			},
			{ 
				id : 'tablet small',
				cols : [
					'col-sm-12',
					'col-sm-10',
					'col-sm-11',
					'col-sm-9',
					'col-sm-8',
					'col-sm-7',
					'col-sm-6',
					'col-sm-5',
					'col-sm-4',
					'col-sm-3',
					'col-sm-2',
					'col-sm-1'
				]
			},
			{ 
				id : 'phone',
				cols : [
					'col-12',
					'col-10',
					'col-11',
					'col-9',
					'col-8',
					'col-7',
					'col-6',
					'col-5',
					'col-4',
					'col-3',
					'col-2',
					'col-1'
				]
			}
		];

	},
	settingsOptions(t){
		const _self = this;

		let data = [
			{ 
				id : 'columns',
				label : 'Number of columns in row',
				type : 'column',
				min : 1,
				value : '',
				placeholder : 'Number of columns in row...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns"]') && document.querySelector('.with-tools.tool-active') ){
						let col = _self.columns();

						document.querySelectorAll('.with-tools.tool-active .col-item').forEach(function(item,index){
							// -- check desktop
							let col_desktop = col.find( i => i.id == 'desktop' ).cols;
							let col_tablet = col.find( i => i.id == 'tablet' ).cols;
							let col_tabletsmall = col.find( i => i.id == 'tablet small' ).cols;
							let col_phone = col.find( i => i.id == 'phone' ).cols;

							for( var i = 0; i < col_desktop.length; i++ ){
								if( item.classList.contains(col_desktop[i]) ){
									document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+index+' .width-default option[value="'+col_desktop[i]+'"]').selected = true;
									break;
								}
							}

							for( var i = 0; i < col_tablet.length; i++ ){
								if( item.classList.contains(col_tablet[i]) ){
									document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+index+' .width-tablet option[value="'+col_tablet[i]+'"]').selected = true;
									break;
								}
							}

							for( var i = 0; i < col_tabletsmall.length; i++ ){
								if( item.classList.contains(col_tabletsmall[i]) ){
									document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+index+' .width-tablet-small option[value="'+col_tabletsmall[i]+'"]').selected = true;
									break;
								}
							}

							for( var i = 0; i < col_phone.length; i++ ){
								if( item.classList.contains(col_phone[i]) ){
									document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+index+' .width-phone option[value="'+col_phone[i]+'"]').selected = true;
									break;
								}
							}

							let cols_count = ( document.querySelectorAll('.with-tools.tool-active .col-item').length > 0 ) ? document.querySelectorAll('.with-tools.tool-active .col-item').length : '';

							document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns"] .column-count').value = cols_count;
							document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns"] .column-count option[value="'+cols_count+'"]').selected = true;;

						});
					}
				},
				save : function(){
					if( document.querySelector('.with-tools.tool-active') ){

						let count = document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns"] .column-count').value;
						
						if( count < document.querySelectorAll('.with-tools.tool-active .col-item').length ){ // -- minus column
							let eq2 = document.querySelectorAll('.with-tools.tool-active .col-item').length - count;
							for( var i = 0; i < eq2; i++ ){
								document.querySelector('.with-tools.tool-active .col-item:last-child').remove();
							}
						}else{ // -- add column
							let eq1 = count-( document.querySelectorAll('.with-tools.tool-active .col-item').length );
							for( var i = 0;i<eq1;i++){
								let el = document.createElement('div');
								el.classList.add('droppable','col-item');
								document.querySelector('.with-tools.tool-active').appendChild(el);
							}

							document.querySelectorAll('.with-tools.tool-active .col-item').forEach(function(item,i){
								let classes = 'droppable col-item ';
								classes+=document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+i+' .width-default').value+' ';
								classes+=document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+i+' .width-tablet').value+' ';
								classes+=document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+i+' .width-tablet-small').value+' ';
								classes+=document.querySelector('#editor-modal .setting[data-settingsid="columns"] .wbtab-content.ref-index'+i+' .width-phone').value+' ';

								item.setAttribute('class',classes);
							}); 
						}

					}

				}

			},
			{ 
				id : 'structure',
				label : 'structure',
				type : 'textarea',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="structure"]') &&  document.querySelectorAll('.with-tools.tool-active') ){

						let html  = ( document.createElement('div') ).appendChild( document.querySelector('.with-tools.tool-active').cloneNode(true) );

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="structure"]').value = html;
					}
				},
				save : function(){

				}

			},
			{ 
				id : 'icon',
				label : 'Icon type https://themify.me/themify-icons, https://fontawesome.com/icons',
				type : 'input',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-10 col-lg-10" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');

						// let icons = activeEl.getAttribute('class').split(' ');
						// let icons_arr = ['fa','fab','fad','fal','far','fas'];
						// let icons_res = false;

						// for( var i = 0; i < icons.length; i ++ ){
						// 	if( icons_arr.includes(icons[i])){
						// 		icons.splice(i,1);
						// 		icons_res = icons.join(' ');
						// 		break;
						// 	}
						// }

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="icon"]').value = activeEl.getAttribute('class');
						
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="icon"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');

						activeEl.setAttribute('class',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="icon"]').value);
	
					}
				}

			},
			{ 
				id : 'height',
				label : 'height (px/%)',
				type : 'input',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');

						let height = ( activeEl.getAttribute('height') ) ? activeEl.getAttribute('height')+'px' : '';
						let styleHeight = ( activeEl.style.height != '' ) ? activeEl.style.height : false;
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]').value = ( styleHeight ) ? styleHeight : height;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');

						let height = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]').value;
						if( document.querySelector('.with-tools.tool-active').classList.contains('element-image') ){
							if( height != '' ){
								document.querySelector('.with-tools.tool-active > img').setAttribute('height',parseInt(height));
							}
						}

						activeEl.style.minHeight = 'initial';
						activeEl.style.height = height;
					}
				}

			},
			{ 
				id : 'width',
				label : 'Width (px/%)',
				type : 'input',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){

					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');

						let width = ( activeEl.getAttribute('width') ) ? activeEl.getAttribute('width')+'px' : '';
						let stylewidth = ( activeEl.style.width != '' ) ? activeEl.style.width : false;
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]').value = ( stylewidth ) ? stylewidth : width;
					}
				},
				save : function(){

					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]') && document.querySelector('.with-tools.tool-active') ){

						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');

						let width = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]').value;
						if( document.querySelector('.with-tools.tool-active').classList.contains('element-image') ){
							if( width != '' ){
								document.querySelector('.with-tools.tool-active img').setAttribute('width',parseInt(width));
							}
						}

						activeEl.style.minHeight = 'initial';
						activeEl.style.width = width;
					}
				}

			},
			{ 
				id : 'img-alt',
				label : 'Alternative',
				type : 'input',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]').value = ( ( document.querySelector('.with-tools.tool-active img').getAttribute('alt') ) ? document.querySelector('.with-tools.tool-active img').getAttribute('alt') : '' );
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active img').setAttribute('alt', document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]').value);
					}
				}

			},
			{ 
				id : 'img-src',
				label : 'Image Source',
				type : 'input',
				value : '',
				placeholder : 'image source...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]').value = ( ( document.querySelector('.with-tools.tool-active img').getAttribute('src') ) ? document.querySelector('.with-tools.tool-active img').getAttribute('src') : '' );
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active img').setAttribute('src', document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]').value);
					}
				}

			},
			{ 
				id : 'td-rowspan',
				label : 'Row span',
				type : 'number',
				value : '',
				min : 0,
				placeholder : 'row span...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="td-rowspan"]').value = ( document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active').hasAttribute('rowspan') ) ? document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active').getAttribute('rowspan') : '';
					}
				},
				save : function(){
					if( document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active') ){
						document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active').setAttribute('rowspan',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="td-rowspan"]').value);
						document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active').classList.remove('td-active');
					}
				}

			},
			{ 
				id : 'td-colspan',
				label : 'Col span',
				type : 'number',
				value : '',
				min : 0,
				placeholder : 'Column span...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="td-colspan"]').value = ( document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active').hasAttribute('rowspan') ) ? document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active').getAttribute('colspan') : '';
					}
				},
				save : function(){
					if( document.querySelector('.with-tools.tool-active[data-elementtype="element-table"]') ){
						document.querySelector('.with-tools.tool-active[data-elementtype="element-table"] td.td-active').setAttribute('colspan',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="td-colspan"]').value);
					}
				}

			},
			{ 
				id : 'columns-table',
				label : 'Number of columns in row',
				type : 'number',
				min : 1,
				value : '',
				placeholder : 'Number of columns in row...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns-table"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns-table"]').value =  document.querySelectorAll('.with-tools.tool-active th').length;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns-table"]') && document.querySelector('.with-tools.tool-active') ){

						let count = document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns-table"]').value;
						
						// -- recreate table head if table header option is checked to prevent errors
						if( !document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"] input[name="table_header"]').checked && !document.querySelector('.with-tools.tool-active table thead') ){
							let el_thead = document.createElement('thead');
							let ths = '<tr>';
							for(var i= 0; i < count; i++){
								ths+='<th></th>';
							}
							ths+='</tr>';
							el_thead.innerHTML = ths;
							document.querySelector('.with-tools.tool-active table').prepend(el_thead);
							
						}

						if( document.querySelectorAll('.with-tools.tool-active table tbody tr').length > 0 ){
							if( count < document.querySelector('.with-tools.tool-active table tbody tr').querySelectorAll('td').length ){ // -- minus column
								let eq2 = document.querySelector('.with-tools.tool-active table tbody tr').querySelectorAll('td').length - count;
								for( var i = 0; i < eq2; i++ ){

									// -- update header
									if( !document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"] input[name="table_header"]').checked ){
										
										if( document.querySelector('.with-tools.tool-active table thead tr th:last-child') ){
											document.querySelector('.with-tools.tool-active table thead tr th:last-child').remove();
										}
									}
									// -- update body
									document.querySelectorAll('.with-tools.tool-active table tbody tr').forEach(function(item){
										if( item.querySelector('td:last-child') ){
											item.querySelector('td:last-child').remove();
										}
									});	
								} 
							}else{ // -- add column
								let eq1 = count - document.querySelector('.with-tools.tool-active table tbody tr').querySelectorAll('td').length;
								for( var i = 0;i<eq1;i++){
									// -- update header
									if( !document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"] input[name="table_header"]').checked ){
										let el = document.createElement('th');
										el.setAttribute('contentEditable',true);
										el.textContent = 'Header '+( document.querySelectorAll('.with-tools.tool-active table thead tr th').length+1);

										document.querySelector('.with-tools.tool-active table thead tr').appendChild(el);
									}
									// -- update body
									document.querySelectorAll('.with-tools.tool-active table tbody tr').forEach(function(item){
										if( item.querySelectorAll('td').length < count ){
											let el = document.createElement('td');
											el.classList.add('droppable');
											item.appendChild(el);
										}
									});
								}
							}
						}
					}
				}

			},
			{ 
				id : 'rows-table',
				label : 'Number of rows in table',
				type : 'number',
				value : '',
				min : 0,
				placeholder : 'Number of rows...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="rows-table"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="rows-table"]').value =  document.querySelectorAll('.with-tools.tool-active table tbody tr').length;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="rows-table"]') && document.querySelector('.with-tools.tool-active') ){

						let count = document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="rows-table"]').value;
						
						if( count < document.querySelectorAll('.with-tools.tool-active table tbody tr').length ){ // -- minus row
							
							let eq2 = document.querySelectorAll('.with-tools.tool-active table tbody tr').length - count;

							for( var i = 0; i < eq2; i++ ){
								// -- update header
								if( document.querySelector('.with-tools.tool-active table tbody tr:last-child') ){
									document.querySelector('.with-tools.tool-active table tbody tr:last-child').remove();
								}
							} 
						}else{ // -- add row

							document.querySelectorAll('.with-tools.tool-active table tbody tr').forEach(function(item){

								if( item.querySelectorAll('td').length < document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns-table"]').value ){
									for( var i= 0; i < ( document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns-table"]').value - item.querySelectorAll('td').length); i++){	

										let td = document.createElement('td');
											td.classList.add('droppable');

										item.appendChild(td);
									}
								}

							});

							

							for( var i = 0; i < parseInt( count ); i++){								

								let tr = document.createElement('tr');

								let tds = '';

								for( var m = 0; m < document.querySelector('#editor-modal #editor-modal-body .setting[data-settingsid="columns-table"]').value; m++ ){
									tds+='<td class="droppable"></td>';
								}

								tr.innerHTML = tds;


								document.querySelector('.with-tools.tool-active table tbody').appendChild(tr);	
							} 

						}
					
					}
				}

			},
			{ 
				id : 'table-header',
				label : '',
				type : 'checkbox',
				text : 'Remove header',
				value : '1',
				wrapper : '<div class="col-12 col-md-12" style="margin-top:8px;margin-bottom:8px">',
				selected : false,
				wrapper : '<div class="col-12 col-md-12 col-lg-12" style="margin-bottom:8px">',
				onedit : function(){

					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"]') && document.querySelector('.with-tools.tool-active') ){
						let activeEl = document.querySelector('.with-tools.tool-active');

						if( activeEl.querySelector('table').classList.contains('no-header') ){
							document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"] input[name="table_header"]').checked = true;
						}else{
							document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"] input[name="table_header"]').checked = false;
						}

						 
					}
				},
				save : function(){

					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"]') && document.querySelector('.with-tools.tool-active') ){
						let activeEl = document.querySelector('.with-tools.tool-active');

						if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="table-header"] input[name="table_header"]').checked ){
							activeEl.querySelector('table').classList.add('no-header');
							if( activeEl.querySelector('table thead') ){
								activeEl.querySelector('table thead').remove();
							}
						}else{
							activeEl.querySelector('table').classList.remove('no-header');
							if( !activeEl.querySelector('table thead') ){
								let el = document.createElement('thead');
								activeEl.querySelector('table').appendChild(el);

								if( activeEl.querySelector('table tbody tr') ){
									activeEl.querySelector('table tbody tr').querySelectorAll('td').forEach( function(item,index) {
										let el = document.createElement('th');
										el.setAttribute('contentEditable',true);
										el.textContent = 'Header '+(index+1);
										activeEl.querySelector('table thead').appendChild(el);
									});
								}
							}
						}
					}
				}
			},
			{ 
				id : 'custom-id',
				label : 'Custom ID',
				type : 'input',
				value : '',
				placeholder : 'Custom ID...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){

					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = '';

						if( elementType === 'col-item' ){
							activeEl = document.querySelector('.with-tools.tool-active');
						}else{
							activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						}

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]').value =  ( activeEl.getAttribute('id') ) ? activeEl.getAttribute('id') : '';
					}
				},
				save : function(){

					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = '';

						if( elementType === 'col-item' ){
							activeEl = document.querySelector('.with-tools.tool-active');
						}else{
							activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						}


						activeEl.setAttribute('id',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]').value) ;
					}
				}
			},
			{ 
				id : 'custom-class',
				label : 'Custom Classes',
				type : 'input',
				value : '',
				placeholder : 'Custom classes...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){

					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]') && document.querySelector('.with-tools.tool-active') ){

						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = '';
						
						if( elementType === 'col-item' ){
							activeEl = document.querySelector('.with-tools.tool-active');
						}else{
							activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						}

						let classes =  ( activeEl.getAttribute('class') ) ? activeEl.getAttribute('class') : '';

						classes = classes.replace('no-header','');
						classes = classes.replace('element-item','');
						classes = classes.replace('element','');
						classes = classes.replace('tool-active','');
						classes = classes.replace('active','');
						classes = classes.replace('droppable','');
						classes = classes.replace('ui-droppable','');
						classes = classes.replace('ui-sortable-handle','');
						classes = classes.replace('ui-sortable','');
						classes = classes.replace('edit-tool','');
						classes = classes.replace('delete-tool','');
						classes = classes.replace('with-tools','');
						classes = classes.replace('element-wrapper','');
						classes = classes.replace('row','');

						if( document.querySelector('.with-tools.tool-active').classList.contains('element-column') ){
							classes = classes.replace('container-fluid','');
							classes = classes.replace('container','');
						}

						_self.toolList().forEach(function(item){
							classes = classes.replace(item.id,'');
						});

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]').value = classes.trim();
					}
				},
				save : function(){
					
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]') && document.querySelector('.with-tools.tool-active') ){

						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = '';
						
						if( elementType === 'col-item' ){
							activeEl = document.querySelector('.with-tools.tool-active');
						}else{
							activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						}
						
						let classes = [];
						let default_classes = [];

						if(  activeEl.classList.contains('no-header') ){
							default_classes.push('no-header');
						}

						if(  activeEl.classList.contains('element-item') ){
							default_classes.push('element-item');
						}

						if(  activeEl.classList.contains('element') ){
							default_classes.push('element');
						}

						if(  activeEl.classList.contains('tool-active') ){
							default_classes.push('tool-active');
						}
						if(  activeEl.classList.contains('droppable') ){
							default_classes.push('droppable');
						}

						if( activeEl.classList.contains('ui-droppable') ){
							default_classes.push('ui-droppable');
						}
						if( activeEl.classList.contains('ui-sortable-handle') ){
							default_classes.push('ui-sortable');
						}

						if( activeEl.classList.contains('ui-sortable') ){
							default_classes.push('ui-sortable-handle');
						}

						if( activeEl.classList.contains('edit-tool') ){
							default_classes.push('edit-tool');
						}

						if( activeEl.classList.contains('delete-tool') ){
							default_classes.push('delete-tool');
						}

						if( activeEl.classList.contains('with-tools') ){
							default_classes.push('with-tools');
						}

						if( activeEl.classList.contains('element-wrapper') ){
							default_classes.push('element-wrapper');
						}

						if( activeEl.classList.contains('row') ){
							default_classes.push('row');
						}

						for( var i = 0; i < _self.toolList().length;i++){
							if ( activeEl.classList.contains(_self.toolList()[i].id) ){
								default_classes.push(_self.toolList()[i].id);
								break;
							}
						}

						let classes2 = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]').value.split(' ');

						classes2.forEach(function(item){
							classes.push(item.trim());
						});

						if( activeEl.getAttribute('class') ){
							activeEl.getAttribute('class').split(' ').forEach(function( item ){
								if( default_classes.indexOf(item) == -1 ){
									activeEl.classList.remove(item);								
								}
							});

							// -- continue
							classes.forEach(function(item){
								if( item.trim() != '' ){
									activeEl.classList.add(item);
								}
							});
						}

					}
				}
			},
			{ 
				id : 'custom-type',
				label : 'Type',
				type : 'select',
				value : 'section',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				options : [
					{
						value : 'section',
						text : 'Section',
						selected : true,
					},
					{
						value : 'div',
						text : 'Div',
						selected : false
					}
				],
				save : function(){

				}
			},
			{ 
				id : 'custom-width',
				label : 'Container width',
				type : 'radio-options',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				options : [
					{ 
						text : 'Full',
						value : 'full',
						wrapper : '<div class="col-12 col-md-3" style="margin-bottom:8px">',
						selected : true
					},
					{ 
						text : 'Fixed',
						value : 'fixed',
						wrapper : '<div class="col-12 col-md-3" style="margin-bottom:8px">',
						selected : false
					}
				],
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"]') && document.querySelector('.with-tools.tool-active') ){
						let v = ( ( document.querySelector('.with-tools.tool-active').classList.contains('container') ) ? 'fixed' : 'full' );
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"] input[value="'+v+'"]').checked = true;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"]') && document.querySelector('.with-tools.tool-active') ){
						let v = ( ( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"] input:checked').value == 'fixed' ) ? 'container' : 'container-fluid' );
						
						document.querySelector('.with-tools.tool-active').closest('.container-wrapper').classList.remove('container','container-fluid');
						document.querySelector('.with-tools.tool-active').closest('.container-wrapper').classList.add(v);
						

					}
				
				}
			},
			{ 
				id : 'custom-css',
				label : 'Custom CSS',
				type : 'textarea',
				value : '',
				placeholder : 'Custom css...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-css"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = '';
						
						if( elementType === 'col-item' ){
							activeEl = document.querySelector('.with-tools.tool-active');
						}else{
							activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						}

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-css"]').value = ( ( activeEl.getAttribute('style') ) ? activeEl.getAttribute('style') : '' );
					}
				},
				save : function(){
					
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-css"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = '';
					
						if( elementType === 'col-item' ){
							activeEl = document.querySelector('.with-tools.tool-active');
						}else{
							activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						}

						activeEl.setAttribute('style',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-css"]').value);
					}
						
					
				}
			},
			{ 
				id : 'custom-style',
				label : 'Custom Style',
				type : 'textarea',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-style"]') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-style"]').value = ( ( document.querySelector('#custom-resources-ref  #custom-style-ref') ) ? document.querySelector('#custom-resources-ref  #custom-style-ref').innerHTML : '' );
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-style"]') ){
						document.querySelector('#custom-resources-ref #custom-style-ref').innerHTML = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-style"]').value;
					}
				}
			},
			{ 
				id : 'custom-js',
				label : 'Custom JS',
				type : 'textarea',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-js"]') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-js"]').value = ( ( document.querySelector('#custom-resources-ref  #custom-js-ref') ) ? document.querySelector('#custom-resources-ref  #custom-js-ref').innerHTML : '' );
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-js"]') ){
						document.querySelector('#custom-resources-ref #custom-js-ref').innerHTML = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-js"]').value;
					}
				}
			},
			{ 
				id : 'custom-html',
				label : 'Custom html',
				type : 'textarea',
				value : '',
				placeholder : 'Custom html...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-html"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-html"]').value = document.querySelector('.with-tools.tool-active .html-wrapper').innerHTML;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-html"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active .html-wrapper').innerHTML = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-html"]').value;
					}
				}
			},
			{ 
				id : 'template-name',
				label : 'Template name',
				type : 'input',
				value : document.querySelector('#template-editor').getAttribute('data-templatename'),
				placeholder : 'Template name...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="template-name"]').value = document.querySelector('#template-editor').getAttribute('data-templatename');
				},
				save : function(){
					document.querySelector('#template-editor').setAttribute('data-templatename',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="template-name"]').value);
				}
			},
			{ 
				id : 'form-action',
				label : 'Form Action',
				type : 'input',
				value : '',
				placeholder : 'Form url...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-action"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-action"]').value = ( ( activeEl.getAttribute('action') ) ? activeEl.getAttribute('action') : '');
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-action"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						activeEl.setAttribute('action',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-action"]').value);
					}
				}

			},
			{ 
				id : 'form-method',
				label : 'Form method (post/get)',
				type : 'select',
				value : 'post',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				options : [
					{
						value : 'post',
						text : 'POST',
						selected : true,
					},
					{
						value : 'get',
						text : 'GET',
						selected : false
					}
				],
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-method"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-method"]').value = ( ( activeEl.getAttribute('method') ) ? activeEl.getAttribute('method') : '');
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-method"] option[value="'+activeEl.getAttribute('method')+'"]').selected = true;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-method"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						activeEl.setAttribute('method',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-method"]').value);
					}
				}
			},
			{ 
				id : 'form-multipart',
				type : 'checkbox',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				text : 'Form multipart?',
				value : 'multipart/form-data',
				wrapper : '<div class="col-12 col-md-3" style="margin-bottom:8px">',
				name : 'form_multipart',
				selected : false,
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-multipart"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						if( activeEl.getAttribute('enctype') ){
							document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-multipart"] input[name="form_multipart"]').checked = true;
						}else{
							document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-multipart"] input[name="form_multipart"]').checked = false;
						}
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-multipart"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //
						
						if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="form-multipart"] input[name="form_multipart"]').checked ){
							activeEl.setAttribute('enctype','multipart/form-data');
						}else{
							activeEl.removeAttribute('enctype');
						}
					}
				
				}
			},
			{ 
				id : 'buttons',
				label : 'Buttons',
				type : 'select',
				value : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				options : [
					{
						value : '',
						text : 'Select button type',
						selected : true,
					},
					{
						value : 'btn-primary',
						text : 'Primary',
						selected : false
					},
					{
						value : 'btn-danger',
						text : 'Danger',
						selected : false
					},
					{
						value : 'btn-warning',
						text : 'Warning',
						selected : false
					},
					{
						value : 'btn-success',
						text : 'Success',
						selected : false
					}
				],
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="buttons"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						let btn_arr = ['btn-primary','btn-danger','btn-wraning','btn-success'];
						let btn = '';

						for( var i = 0; i < btn_arr.length; i++ ){
							if( activeEl.classList.contains(btn_arr[i]) ){
								btn = btn_arr[i];
								break;
							}
						} 

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="buttons"]').value = btn;
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="buttons"] option[value="'+btn+'"]').selected = true;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="buttons"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						let btn_arr = ['btn-primary','btn-danger','btn-wraning','btn-success'];

						activeEl.classList.remove(...btn_arr);

						if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="buttons"]').value != '' ){
							activeEl.classList.add(document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="buttons"]').value);
						}
					}
				}
			},
			{ 
				id : 'custom-editor',
				label : 'Text Editor',
				type : 'textarea',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-editor"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-editor"]').value = activeEl.innerHTML;

						setTimeout(function(){
							tinymce.init({ selector:'#editor-modal .editor-modal-body .setting[data-settingsid="custom-editor"]',save_enablewhendirty: true});
						},300);
					}
				},
				save : function(){
					
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-editor"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type; // -- to be continue
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active'); //

						activeEl.innerHTML = tinymce.activeEditor.getContent();
					}
				}
			},
			{ 
				id : 'option-select',
				label : 'Options',
				type : 'option-select',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="option-select"]') && document.querySelector('.with-tools.tool-active') ){
						let html = '';
						document.querySelectorAll('.with-tools.tool-active select option').forEach(function( item ){
							let selected = ( item.selected ) ? 'checked' : '';
							html+='<tr><td><i class="delete-option ti-trash"></i></td><td class="text" contentEditable="true">'+item.textContent+'</td><td class="value" contentEditable="true">'+item.value+'</td><td class="selected"><input type="radio" name="option_selected" value="1" '+selected+'></td></tr>';
						});

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="option-select"] tbody').innerHTML = html;

						setTimeout(function(){
							document.querySelector('#editor-modal .editor-modal-body .add-option').addEventListener('click',function(){	
								let newel = document.createElement('tr');
									newel.innerHTML = '<tr><td><i class="delete-option ti-trash"></i></td><td class="text" contentEditable="true"></td><td class="value" contentEditable="true"></td><td class="selected"><input type="radio" name="option_selected" value="1"></td></tr>';
									newel.querySelector('.delete-option').addEventListener('click',function(){
										this.closest('tr').remove();
									});
								document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="option-select"] tbody').appendChild(newel);
							});
							document.querySelectorAll('#editor-modal .editor-modal-body .delete-option').forEach(function( item ){
								item.addEventListener('click',function(){
									this.closest('tr').remove();
								});
							});
						},300);
					}
				},
				save : function(){
					
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="option-select"]') && document.querySelector('.with-tools.tool-active') ){
						let html = '';
						document.querySelectorAll('#editor-modal .editor-modal-body .setting[data-settingsid="option-select"] tbody tr').forEach(function(item){
							let selected = ( item.querySelector('.selected input').checked ) ? 'selected' : '';
							html+='<option value="'+item.querySelector('.value').textContent+'" '+selected+'>'+item.querySelector('.text').textContent+'</option>';
						});

						document.querySelector('.with-tools.tool-active select').innerHTML = html;
					}
				}
			},
			{ 
				id : 'custom-resources',
				label : 'Custom Resources',
				type : 'custom-resources',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-resources"]') ){
						document.querySelectorAll('#custom-resources-ref .custom-resources-header li').forEach(function(item){
							let el = document.createElement('li');
							el.setAttribute('data-link',item.textContent);
							el.innerHTML = item.textContent+` <span class="badge badge-danger remove" onclick="this.closest('li').remove()">remove</span>`;
							document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-resources"] .wbtab-content .custom-resources.custom-resources-header').appendChild(el);
						});
						document.querySelectorAll('#custom-resources-ref .custom-resources-footer li').forEach(function(item){
							let el = document.createElement('li');
							el.setAttribute('data-link',item.textContent);
							el.innerHTML = item.textContent+` <span class="badge badge-danger remove" onclick="this.closest('li').remove()">remove</span>`;
							document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-resources"] .wbtab-content .custom-resources.custom-resources-footer').appendChild(el);
						});
					}
				},
				save : function(){
					
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-resources"]') ){
						
						document.querySelector('#custom-resources-ref ul.custom-resources-header').innerHTML = '';
						document.querySelector('#custom-resources-ref ul.custom-resources-footer').innerHTML = '';
						
						
						// populate the header resources ref
						document.querySelectorAll('#editor-modal .editor-modal-body .setting[data-settingsid="custom-resources"] .custom-resources-header li').forEach(function(item){
							let el = document.createElement('li');
							el.textContent = item.getAttribute('data-link');
							document.querySelector('#custom-resources-ref ul.custom-resources-header').appendChild(el);
						})
						// populate the footer resources ref
						document.querySelectorAll('#editor-modal .editor-modal-body .setting[data-settingsid="custom-resources"] .custom-resources-footer li').forEach(function(item){
							let el = document.createElement('li');
							el.textContent = item.getAttribute('data-link');
							document.querySelector('#custom-resources-ref ul.custom-resources-footer').appendChild(el);
						});

					}
				}
			},
			{ 
				id : 'custom-text',
				label : 'Text',
				type : 'input',
				value : '',
				placeholder : 'Custom text',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-text"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-text"]').value = activeEl.textContent;
					}
				},
				save : function(){
					
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-text"]') && document.querySelector('.with-tools.tool-active') ){
						let elementType = _self.toolList().find( i => i.id == document.querySelector('.with-tools.tool-active').getAttribute('data-elementtype') ).element_type;
						let activeEl = ( document.querySelector('.with-tools.tool-active').classList.contains('element-wrapper') ) ? document.querySelector('.with-tools.tool-active > '+elementType) : document.querySelector('.with-tools.tool-active');
						activeEl.textContent = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-text"]').value;
					}
				}
			}  
		];

		return ( data.findIndex( i => i.id == t ) != -1 ) ? data.find( i => i.id == t ) : false;
	}
}