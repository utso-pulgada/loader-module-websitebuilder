import $ from './jqueryui/jquery.js';
import './jqueryui/jquery-ui.min.css';
import './jqueryui/jquery-ui.min.js';
import elements from './elements.js'; // -- html interface
import elementsParser from './elementsParser.js'; // -- elements parser

export default function( obj ){
	if( $(obj).length > 0 ){
      if( $(obj).hasClass('element-column') ){
        $(obj).closest('.container-wrapper').sortable({
          appendTo: $('#template-editor .editor-body'),
          receive: function( event, ui ) {
            
          },
          snapTolerance: 30,
          cancel: '.tools'
        }).click(function(){
            return;
        }).dblclick(function() {
          if( $(this).hasClass('element-paragraph') ){

            $(this).addClass('tool-active')

            elementsParser.settings(['custom-text']);
          
            setTimeout(function(){
                elements.settingsOptions(['custom-text']).onedit();
            },100);

            document.querySelector('#editor-modal').setAttribute('data-settingsid','default');
          }

          if( $(this).hasClass('element-select') ){

            $(this).addClass('tool-active')

            elementsParser.settings(['option-select']);
          
            setTimeout(function(){
                elements.settingsOptions(['option-select']).onedit();
            },100);

            document.querySelector('#editor-modal').setAttribute('data-settingsid','default');
          }

          return;
        });
        
      }else{
        $(obj).sortable({
          appendTo: $('#template-editor .editor-body'),
          receive: function( event, ui ) {
          },
          snapTolerance: 30,
          cancel: '.tools'
        }).click(function(){
            return;
        }).dblclick(function() {

          if( $(this).hasClass('element-paragraph') ){

            $(this).addClass('tool-active')

            elementsParser.settings(['custom-text']);
          
            setTimeout(function(){
                elements.settingsOptions(['custom-text']).onedit();
            },100);

            document.querySelector('#editor-modal').setAttribute('data-settingsid','default');
          }


          if( $(this).hasClass('element-select') ){

            $(this).addClass('tool-active')

            elementsParser.settings(['option-select']);
          
            setTimeout(function(){
                elements.settingsOptions(['option-select']).onedit();
            },100);

            document.querySelector('#editor-modal').setAttribute('data-settingsid','default');
          }


          return;


        });
      }
    }
}