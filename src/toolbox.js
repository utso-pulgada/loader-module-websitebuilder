import PouchDB from 'pouchdb-browser';
import $ from './jqueryui/jquery.js';
import elements from './elements.js'; // -- html interface
import makeid from './makeid.js' // -- create random hash module
import modal from './modal.js'; // -- modal module
import elementsParser from './elementsParser.js';
import droppable from './droppable.js'; // -- droppable
import toolEvents from './events.js'; // -- module events
import draggable from './draggable.js'; // -- draggable
import updateElement from './updateElement.js'; // -- update template to database

const entities = require("entities");

let db = new PouchDB('websiteeditor');

export default{
	templateDroppable(){
		droppable( document.querySelector('.editor-body.droppable') );
	},
	toolSidebar(){
		let el = document.createElement('div');
			el.classList.add('editor-tool');
			el.innerHTML = '<a href="#" class="editor-tool-trigger"><i class="ti-plus"></i></a><a href="#" class="editor-tool-settings" style=" top: 65px; width: 40px; height: 40px; right: -65px;"><i class="ti-settings"></i></a><div class="editor-tool-body wb-main-wrapper"><div style="overflow-y:auto;overflow-x:hidden;height:100%;padding:0px 24px;">'+elementsParser.sidebarAccordions()+'</div></div>';
			el.querySelector('.editor-tool-trigger').addEventListener('click',function(){
				if( this.closest('.editor-tool').classList.contains('active') ){
					this.closest('.editor-tool').classList.remove('active');
				}else{
					this.closest('.editor-tool').classList.add('active');
				}
			});

			el.querySelector('.editor-tool-settings').addEventListener('click',function(){
				elementsParser.settings(['template-name','custom-style','custom-js','custom-resources']);
				setTimeout(function(){
					elements.settingsOptions('custom-style').onedit();
					elements.settingsOptions('custom-js').onedit();
					elements.settingsOptions('template-name').onedit();
					elements.settingsOptions('custom-resources').onedit();
				},500)
				document.querySelector('#editor-modal').setAttribute('data-settingsid','template');
			});


		document.querySelector('body').appendChild(el);

		$( ".draggable" ).draggable({
			start : function(event,ui){
				event.target.style.background = '#ededed';
			  	if( document.querySelector('.editor-tool.active') ){
					document.querySelector('.editor-tool.active').classList.remove('active');
				}
			},
			cursor: "crosshair",
			helper: "clone"
		});
	},
	tool( t ){
		const _self = this;

		let el = $('.droppable.active');


		if( el.length == 0 ){ // if not found then abort
			console.log('no active item found');
			return;
		}


		if( elements.toolList().findIndex( i => i.id == t ) == -1 ){
			return;
		}


		let html = ( elements.toolList().findIndex( i => i.id == t ) != -1 ) ? elements.toolList().find( i => i.id == t ) : '';

		let template_id = $('#template-editor').attr('data-templateid');

		db.get( template_id ).then(function(doc){
			let elementid = 'element-'+makeid(6)+doc.contents.length;
		
			let newel = document.createElement('div');
				newel.innerHTML = html.contents;
				newel.querySelector('.element').setAttribute('data-elementid',elementid);
				newel.querySelector('.element').setAttribute('data-elementtype',html.id);

			el.append(newel.innerHTML);

			if( document.querySelector('.element[data-elementid="'+elementid+'"][data-elementtype="element-table"]') ){
				document.querySelector('.element[data-elementid="'+elementid+'"][data-elementtype="element-table"]').querySelectorAll('tbody tr td').forEach( function( item ){
					item.addEventListener( 'click', function(){
						
						this.closest('.element[data-elementtype="element-table"]').classList.add('tool-active');

						if( document.querySelector('td.td-active') ){
							document.querySelector('td.td-active').classList.remove('td-active');
						}
						this.classList.add('td-active');
						elementsParser.settings(['td-rowspan','td-colspan']);
					});
				});
			}

			let raw_html = el.html();

			let contents = _self.buildContents();

			return db.put({
				_id : doc._id,
				_rev : doc._rev,
				templateid : doc.templateid,
				template_name : document.querySelector('#template-editor').getAttribute('data-templatename'),
				custom_header : doc.custom_header,
				custom_footer : doc.custom_footer,
				contents : contents,
				created_at : doc.created_at,
				updated_at : ( ( new Date() ).getTime() ).toString()
			}).then(function(){

				toolEvents.toolEvents(document.querySelector('.droppable.active'));

				droppable(document.querySelector('.droppable.active'));

				// -- activate draggable
				draggable('.element[data-elementid="'+elementid+'"]');

				$('[contentEditable="true"]').off('dblclick').dblclick(function(){
					$(this).attr('contentEditable',true).focus();
				});

				// trigger columns settings
				if( elements.toolList().findIndex( i => i.id == t ) != -1 && Array.isArray( elements.toolList().find( i => i.id == t ).activate_settings ) ){

					$('.with-tools').removeClass('tool-active active');
					
					$('.element[data-elementid="'+elementid+'"]').addClass('tool-active');

					elementsParser.settings(elements.toolList().find( i => i.id == t ).activate_settings);

					setTimeout(function(){
						elements.toolList().find( i => i.id == t ).activate_settings.forEach(function(item){
							elements.settingsOptions(item).onedit();
						});
					},500);
				}

			});
		}).catch(function(err){
			console.log(err);
			console.log('Unable to retrieve template '+template_id);
		});
	},
	buildContents(){
		let el = document.createElement('div');
		el.innerHTML = document.querySelector('.editor-body').innerHTML;

		el.querySelectorAll('.with-tools').forEach(function(item){
			item.querySelectorAll('.tools').forEach(function(item2){
				item2.remove();
			});
			item.classList.remove('tool-active','active','droppable','sortable','ui-droppable','ui-sortable','ui-sortable-handle','drop-hover','ui-state-highlight');
		});

		return entities.encodeHTML(el.innerHTML);	
	},
	getTemplates(){
	
	},
	previewTemplate(){
		
	},
	saveTemplate(){
		updateElement();
	},
	createTemplate(){

		let templateid = $('#template-editor').attr('data-templateid');
		let templatename = $('#template-editor').attr('data-templatename');

		db.get( templateid ).then(function(doc){

			// -- clean
			let newel = document.createElement('div');
				newel.innerHTML = entities.decodeHTML(doc.contents);

				newel.querySelectorAll('.element').forEach(function(item){
					item.removeAttribute('contentEditable');
					item.querySelectorAll('[contentEditable]').forEach(function(item2){
						item2.removeAttribute('contentEditable');
					});
					item.removeAttribute('data-elementid');
					item.removeAttribute('data-elementtype');
					item.querySelectorAll('.tools').forEach(function(item2){
						item2.remove();
					});
				});

			
			let data = {
				template_id : doc.templateid,
				template_name : doc.template_name,
				custom_style : entities.decodeHTML(doc.custom_css),
				custom_js  : entities.decodeHTML(doc.custom_js),
				production_html : newel.innerHTML,
				editable_html : doc.contents,
				created_at : doc.created_at,
				updated_at : doc.updated_at
			}

			$.post(window.websitebuilder_config.url,data,function(res){
				console.log(res);
			}).fail(function(){
				alert('Unable to connect to server');
			});

		}).catch(function(err){
			console.log(err);
			console.log('Unable to retrieve template '+templatename);
			modal(true,'<p style="color:red"><i class="ti-alert" style="margin-right:8px;"></i>Unable to retrieve template '+templatename+'</p>');
		});

	}
	
}